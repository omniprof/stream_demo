package com.winterbe.stream_demo;

import java.util.OptionalInt;
import java.util.stream.IntStream;

/**
 * @author Benjamin Winterberg
 */
public class Streams04 {

    public void perform() {

    	System.out.println("Old fashioned loop::");
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }

        System.out.println("\nforEach lambda:");
        IntStream.range(0, 10)
            .forEach(i -> {
                if (i % 2 == 1) System.out.println(i);
            });

        System.out.println("\nforEach filter:");
        IntStream.range(0, 10)
            .filter(i -> i % 2 == 1)
            .forEach(System.out::println);

        System.out.println("\nOptional reduce:");
        OptionalInt reduced1 =
            IntStream.range(0, 10)
                .reduce((a, b) -> a + b);
        System.out.println(reduced1.getAsInt());

        System.out.println("\nreduce:");
        int reduced2 =
            IntStream.range(0, 10)
                .reduce(7, (a, b) -> a + b);
        System.out.println(reduced2);
    }
    
    public static void main(String[] args) {
        Streams04 s4 = new Streams04();
        s4.perform();
        System.exit(0);
    }
    
}
