package com.winterbe.stream_demo;

import java.util.Arrays;

/**
 * @author Benjamin Winterberg
 */
public class Streams09 {

    public static void main(String[] args) {
        Streams09 s9 = new Streams09();
        s9.perform();
        System.exit(0);
    }

    public void perform() {
        Arrays.asList("a1", "a2", "b1", "c2", "c1")
                .stream()
                .filter(s -> s.startsWith("c"))
                .map(String::toUpperCase)
                .sorted()
                .forEach(System.out::println);

        // C1
        // C2
    }
}
