package com.winterbe.stream_demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Benjamin Winterberg
 */
public class Streams02 {

    public void perform(){

        List<String> stringCollection = new ArrayList<>(Arrays.asList("ddd2","aaa2","bbb1","aaa1","bbb3","ccc","bbb2","ddd1"));
//        stringCollection.add("ddd2");
//        stringCollection.add("aaa2");
//        stringCollection.add("bbb1");
//        stringCollection.add("aaa1");
//        stringCollection.add("bbb3");
//        stringCollection.add("ccc");
//        stringCollection.add("bbb2");
//        stringCollection.add("ddd1");


        // sorting
        System.out.println("\nsorted stream:");
        stringCollection
                .stream()
                .sorted()
                .forEach(System.out::println);

        System.out.println("\noriginal collection after sorted stream:");
        System.out.println(stringCollection);

        

    }

    public static void main(String[] args) {
        Streams02 s2 = new Streams02();
        s2.perform();
        System.exit(0);
    }
}
