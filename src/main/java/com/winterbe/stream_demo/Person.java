package com.winterbe.stream_demo;

/**
* @author Benjamin Winterberg
*/
class Person {
    String firstName;
    String lastName;

    Person() {}

    Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}